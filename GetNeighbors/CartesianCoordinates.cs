﻿using System;
#pragma warning disable

namespace GetNeighbors
{
    public static class CartesianCoordinates
    {
        /// <summary>
        /// Gets from a set of points only points that are h-neighbors for a point with integer coordinates x and y.
        /// </summary>
        /// <param name="point">Given point with integer coordinates x and y.</param>
        /// <param name="h">Distance around a given point.</param>
        /// <param name="points">A given set of points.</param>
        /// <returns>Only points that are h-neighbors for a point with integer coordinates x and y.</returns>
        /// <exception cref="ArgumentNullException">Throw when array points is null.</exception>
        /// <exception cref="ArgumentException">Throw when h-distance is less or equals zero.</exception>

        public static bool IsNeighbor(Point centre, int h, Point point)
        {
            return (Math.Abs(centre.X - point.X) <= h && Math.Abs(centre.Y - point.Y) <= h);
        }
        public static Point[] GetNeighbors(Point point, int h, params Point[] points)
        {
            if (points == null) throw new ArgumentNullException(nameof(points));
            if (h <= 0) throw new ArgumentException(string.Empty, nameof(h));

            Point[] neighbors = new Point[points.Length];
            int j = 0;
            for(int i = 0; i < points.Length; i++)
            {
                if(IsNeighbor(point,h,points[i]))
                {
                    neighbors[j] = points[i];
                    j++;
                }
            }
            Point[] ans = new Point[j];
            for (int i = 0; i < j; i++)
            {
                ans[i] = neighbors[i];
            }
            return ans;

        }




    }
}
